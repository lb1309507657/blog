<?php
// 支付宝的配置
return [
    'use_sandbox' => true,// 是否使用沙盒模式

    'app_id' => '2016092900619910',
    'sign_type' => 'RSA2',// RSA  RSA2

    // ！！！注意：如果是文件方式，文件中只保留字符串，不要留下 -----BEGIN PUBLIC KEY----- 这种标记
    // 可以填写文件路径，或者密钥字符串  当前字符串是 rsa2 的支付宝公钥(开放平台获取)
    'ali_public_key' =>"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArTzDKa5Brl2wZ24gbQHDqOjJI7Aw9ZK50Ox+9ljz7T0WDK8RCJPfd14XX2I0TIwo9m0TCGfM040AfGoRR44x2gORHe0F60Fkrj7bhU/uQBA0G46Tl60YZ+26uqeclbEFXdZeszSjdedZy4fbvYqGCXZ0/b66NWXoXic4shmwdTDBIfKZnBQbN3AK/iHw6BV29soAXMdbOYB4E/KctK31hKXEZJLaOmVFnNhJ8vfa+8eg+XyGMzVQjdfXcTKx8IqGbezD8/UWz/DHC8bGoXxhrhCzihgFkAYVlwAQwiv8d+Ek64Bse7QExEDdwjqLw4F08kb+aD6J9MJ8hgZBx7jvywIDAQAB",
    // ！！！注意：如果是文件方式，文件中只保留字符串，不要留下 -----BEGIN RSA PRIVATE KEY----- 这种标记
    // 可以填写文件路径，或者密钥字符串  我的沙箱模式，rsa与rsa2的私钥相同，为了方便测试
    'rsa_private_key' => 'MIIEowIBAAKCAQEAzAR6NWjxwQ55Qs6Tx8DBF316QLnuVc1vv8qBWTh8fXYXEWp6VI8Oh6wO4Z51WEESXaQ01sScvMWm1GBYSAkCBoyY+lPBzuZainaKjsVysGhZfOiQItF4f3hHGV34EmrtJUDMuo+Q4rTx7bAsNoYzJN0kkZmtvxPMopqvlp3Jodm2hhVxcU1831Czl0EHnP9D+1reJYPKFYtlMuBZkJoap/geOCxaYTg4Syo8qGOOqX69+n5yLLI+oskqN6minyd/RiSe0JkuJdSY5RBlH6hKO8nFsk/I66VZxQUWfscU7ewdQoLpLNdIK5atyZiGsoRxiMB7jVPj85WufXzabpwxtwIDAQABAoIBABu+QCAjNTzbiwHatZ3kSF25YnaK0i440YlzMEPBuqwU8u4bXKdDDObGLv/fXHWz45t4SPcrpWTGO/mFYHvrPatGU0c3PJO2H3nUxOLFVuNGTWT++xrBAXHUGVcJwEJA6B64wR5dh4ZMtNltq5V+ilXo/KAQFeB2AM3b2yTQMCsiAdRkOV+Q8gByFEyQ082CNQymExQ2KGFMxaWg9A58MdICeykgGusf+wmMIYSpAgXjuJacMjIGohgRbGWLTbV1PgLMmQaRXuxCHq2YDfM0n0rqeGj53jsh6RTIGmCUA6GQAjgUgUh7AF6oU9KSBsaq2KeesL5T8Cn0+Ga0gyYoNAECgYEA6asibBGHFFX8AhASP9s8X+LnpOS6aSjacJ5LoqkWStcmXfpwx9UVJxn+Nu7RLTUmGj4jFuO2GKGtJx9mRV5VI4IXY6cG8PD3LXKUUt4Hz3zP1/9Ufd3V4i0l6hBesY9/Y+6F0mpaOMypOey6IpXGxn/cPq9DgFWw0SQhZQ9ToGECgYEA34Po+VDFcbBHsDArjW9RwTLJoyBZVThgyNlVAsFcRuetEFav91aCfOzxy1UTVnSkDvyQavZ2SPi8STbV4oRMg4U9nAxRRu0i9yzkEe7S9tpFqvUZa4rRpObjyzSLMn2SwzUAfy05Pm7Djqqaj4ZnSCYYZHqnpHtGsVnn0IbxaRcCgYEAg3cNPs/9t6tIxcBVBChMp4X53gPx/QhHHeVxmGqZaDH0HBuzKDryU7WEGVPzzAODFHCxPUeQQ/tyd/hqicyxCfE/R4nJlfV8m81DNfxFWK9YIKY+2zHcDAxqsG3VWQDrI0YndGLxr9M1wqk1cA/kbfmx6HKi/6k2efRh9YHSAwECgYB34Tn9LjMXzOJGPy+8xe3lXyYClgET0g8m6lnMBoVFilCqKLRsRjvqYPM2iUS75Adx+khC9jlu3emuoviIa0jWaYonvfuBvQI57BvO+nav61XeYWLm6SeT+DpI7L+TrwIeLLk9fLXtJF9Cj4IiZIXUjooaqiZr86pwyLWqhBXKtQKBgEuznLbGmhpzv3Bmy7VXK7Mdh+cvPJoK4OThHYHULZwaAq04G+L+vqLs7NDsAEKF015/Vr5QjpnJ7ai8unvZm8Qfwg8Sty/gfCaTcDfzNpMiya3lHVjngcl7IEtFV4dycDqtw7nM15DqfU3TCd6V5LGnMrztrK0EjwsS0KYvIcFu',

    'limit_pay' => [
        //'balance',// 余额
        //'moneyFund',// 余额宝
        //'debitCardExpress',// 	借记卡快捷
        //'creditCard',//信用卡
        //'creditCardExpress',// 信用卡快捷
        //'creditCardCartoon',//信用卡卡通
        //'credit_group',// 信用支付类型（包含信用卡卡通、信用卡快捷、花呗、花呗分期）
    ],// 用户不可用指定渠道支付当有多个渠道时用“,”分隔

    // 与业务相关参数
    'notify_url' => 'http://rph7cc.natappfree.cc/six-forum/public/index.php/home/dashang/notify/',
    'return_url' => 'http://www.sixstaredu.com',

    'return_raw' => false,// 在处理回调时，是否直接返回原始数据，默认为 true
];

