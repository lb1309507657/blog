<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Role extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "pri_id" => "array",
        "role_name" =>"require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "pri_id.array" => "请选择权限",
        "role_name.require" => "角色名称不能为空",
    ];
}
