<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Article extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "title"  => "require",
        "summary" => "require",
        "content" => "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     * @var array
     */	
    protected $message = [
        "title.require" => "文章的标题不能为空",
        "summary.require" => "文章的摘要不能为空",
        "content.require" => "文章的内容不能为空",
    ];

    protected $scene = [
        "add" => ["title", "summary", "content"],

    ];
}
