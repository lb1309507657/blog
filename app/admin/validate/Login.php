<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

/**
 * 登录验证器
 * Class Login
 * @package app\admin\validate
 */
class Login extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "username" => ["require", "length:4,16", "regex" => "^[a-zA-Z0-9_-]{4,16}$"],
        "password" => ["require"],
        "captcha" =>["require","captcha"],
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "username.require" => "用户名不能为空",
        "username.length" => "长度要在4到16个字符之间",
        "username.regex" => "名称不合法",
        "password.require" => "密码不能为空",
        "captcha.require" => "验证码不能为空",
        "captcha.captcha" => "验证码错误",
    ];
}
