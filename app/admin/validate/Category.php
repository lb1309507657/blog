<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Category extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "category_name"  => "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "category_name.require"=> "分类名称不能为空",
    ];

    protected $scene = [
        "add" => ["category_name"],
    ];
}
