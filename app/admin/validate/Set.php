<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Set extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    // 基本选项
	    "siteTitle" => "require",
        "siteIcp" => "require",
        // 注册选项
        "siteStatus" => "require",
        "closeContent" => "require",
        // 邮件
        // 主题
        "smtpHost"  => "require",
        "smtpPort"  => "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        // 基本选项
        "siteTitle.require" => "网站的标题不能为空",
        "siteIcp.require" => "网站的备案号不能为空",
        // 注册选项
        "siteStatus.require" => "网站的状态不能为空",
        "closeContent.require" => "网站的关闭时的内容不能为空",
        // 邮箱
        "smtpHost.require" => "邮箱服务器不能为空",
        "smtpPort.require" => "邮箱服务器端口不能为空",
    ];

    /**
     * 定义验证场景
     * @var array
     */
    protected $scene = [
        "base" => ["siteTitle", "siteIcp"],
        "reg" => ["siteStatus", "closeContent"],
        "email" => ["smtpHost", "smtpPort"],
    ];
}
