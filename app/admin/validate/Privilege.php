<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Privilege extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "parent_id" => "require",
	    "pri_name" => "require",
	    "app_name" => "require",
	    "controller_name" => "require",
	    "action_name" => "require",
        "id" => "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "parent_id.require"  => "没有选择父级权限",
        "pri_name.require"  => "权限名称不能为空",
        "controller_name.require"  => "控制器名称不能为空",
        "app_name.require"  => "应用名称不能为空",
        "action_name.require"  => "方法名称不能为空",
        "id.require"  => "主键不能为空",
    ];

    // 定义场景
    protected $scene = [
        'edit'  =>  ["parent_id", "pri_name", "controller_name", "app_name", "action_name", "id"],
        "add"  => ["parent_id", "pri_name", "controller_name", "app_name", "action_name"]
    ];
}
