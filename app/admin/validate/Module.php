<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Module extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    // 添加
	    "name" => "require",
        "introduce" => "require",
        // 修改
        "id" => "require",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "name.require" => "版块的名称不能为空",
        "introduce.require" => "版块的描述不能为空",
        "id.require"  => "必须选择修改的模块",
    ];

    // 定义场景
    protected $scene = [
        "add" => ["name", "introduce"],
        "edit" => ["name", "introduce", "id"],
    ];
}
