<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\facade\Log;
use app\admin\model\Options;

/**
 * 网站设置
 * Class Set
 * @package app\admin\controller
 */
class Set extends Base
{
    // 网站的基本设置
    public function base(Request $request)
    {
        if ($request->isAjax()) {
            $base = $request->post();
            $result = (new Options())->addbase($base);
            return json($result);
        }

        return View::fetch();
    }

    // 网站的注册选项
    public function reg(Request $request)
    {
        if ($request->isAjax()) {
            $reg = $request->post();
            $result = (new Options())->addreg($reg);
            return json($result);
        }
        return View::fetch();

    }

    public function theme(Request $request)
    {
        $primaryList= [ // 允许使用的主题色
            '姨妈红'=> 'red',
            '少女粉'=> 'pink',
            '基佬紫'=> 'purple',
            '胖次蓝'=> 'blue',
            '早苗绿'=> 'green',
            '伊藤橙'=> 'orange',
            '呆毛黄'=> 'yellow',
            '远坂棕'=> 'brown',
            '靛'=> 'indigo',
            '青'=> 'cyan',
            '水鸭'=> 'teal',
            '性冷淡'=> 'grey',
        ];
        $accentList=[ // 允许使用的强调色
            '姨妈红'=> 'red',
            '少女粉'=> 'pink',
            '基佬紫'=> 'purple',
            '胖次蓝'=> 'blue',
            '早苗绿'=> 'green',
            '伊藤橙'=> 'orange',
            '呆毛黄'=> 'yellow',
            '靛'=> 'indigo',
            '青'=> 'cyan',
        ];
        $layoutList=[ // 允许使用的模式
            '日间模式'=> 'light',
            '夜间模式'=> 'black',
        ];


        // 数据保存
        if ($request->isAjax()) {
            $theme = $request->post();
            $result = (new Options())->addtheme($theme);
            return json($result);
        }

        // 模板赋值和渲染
        View::assign([
            "primaryList" => $primaryList,
            "accentList" => $accentList,
            "layoutList"  => $layoutList

        ]);
        return View::fetch();

    }

    // 邮件设置
    public function email(Request $request)
    {
        if ($request->isAjax()) {
            $email = $request->post();
            $result = (new Options())->addemail($email);
            return json($result);
        }
        return View::fetch();
    }


    // 邮件发送测试
    public function sendtest()
    {

    }


    // 保存邮件模板
    public function savetemp(Request $request)
    {
        $temp = $request->post();
        Log::write($temp);
    }

}
