<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\facade\Log;
use app\admin\model\Module as Mmodule;
use app\admin\controller\Base;

/**
 * 版块管理
 * Class Module
 * @package app\admin\controller
 */
class Module extends Base
{
    /**
     * 显示版块列表
     */
    public function modulelist()
    {
        // 获取模块列表
        $mlist = (new Mmodule())->modulelist();
        $mtree = make_tree($mlist);

        // 赋值和渲染
        View::assign([
            "mlist" => $mlist,
            "mtree" => $mtree
        ]);
        return View::fetch();
    }

    // 添加版块
    public function addmodule(Request $request)
    {
        $module = $request->post();
        $result = (new Mmodule())->addmodule($module);
        return json($result);
    }

    // 更新模块
    public function updatemodule(Request $request)
    {
        $module = $request->post();
        $result = (new Mmodule())->updatemodule($module);
        return json($result);
    }
}
