<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\facade\Log;
use app\admin\model\Privilege as Pri;
use think\facade\Db;
use app\admin\controller\Base;

/**
 * 权限管理
 * Class Privilege
 * @package app\admin\controller
 */
class Privilege extends Base
{
    /**
     * 显示权限列表
     *
     * @return \think\Response
     */
    public function index()
    {

        // 获取权限列表
        $listpri = (new Pri())->listpri();
        foreach ($listpri as &$pri) {
            if ($pri["parent_id"] == "0") {
                $pri["parent_name"] = "顶级权限";
            } else {
                $pri["parent_name"] = Db::name("privilege")->where("id",$pri["parent_id"])->value("pri_name");
            }
        }

        // 生成有层级的数据
        $result = \make_tree($listpri);



        // 模板的赋值和渲染
        View::assign([
            "listpri" =>$listpri,
            "result" => $result
        ]);

        return View::fetch();
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 添加权限
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        // 获取权限数据
        $pri = $request->post();
        $result = (new Pri())->savepri($pri);
        return json($result);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 更新权限
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        // 获取权限数据
        $pri = $request->post();
//        Log::write($pri);
        $result = (new Pri())->editpri($pri);
        return json($result);
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delpri(Request $request)
    {
        // 获取权限的id
        $pid = $request->post()["id"];
        $result = (new Pri())->delpri($pid);
        return json($result);
    }
}
