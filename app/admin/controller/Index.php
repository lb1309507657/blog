<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use app\admin\controller\Base;

/**
 * 后台首页
 * Class Index
 * @package app\admin\controller
 */
class Index extends Base
{
    // 首页
    public function index(Request $request)
    {
        // 获取系统信息
        $serverinfo =[
            "操作系统"    =>PHP_OS,
            "运行环境"    =>($request->server())["SERVER_SOFTWARE"],
            "主机名"      =>$request->host(),
            "WEB服务端口" =>$request->port(),
            "网站文档目录" =>$request->root(),
            "浏览器信息"  =>($request->server())["HTTP_USER_AGENT"],
            "通信协议"    => $request->protocol(),
            "请求方法"    => $request->method(),
        ];

        // 模板赋值和渲染
        View::assign([
            "serverinfo" => $serverinfo,
        ]);
        return View::fetch();
    }
}
