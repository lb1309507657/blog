<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use app\admin\model\Privilege as Pri;
use think\facade\Log;
use app\admin\model\Role as Ro;
use app\admin\controller\Base;

/**
 * 角色管理
 * Class Role
 * @package app\admin\controller
 */
class Role  extends Base
{
    /**
     * 显示角色列表
     *
     * @return \think\Response
     */
    public function index()
    {
        // 获取权限列表
        $listpri = (new Pri())->listpri();
        $pritree = \make_tree($listpri);

        // 获取角色列表
        $rolelist = (new Ro())->rolelist();
        // 模板赋值和渲染
        View::assign([
            "pritree" => $pritree,
            "rolelist"=> $rolelist,
        ]);
        return View::fetch();
    }

    /**
     * 添加角色
     * @param Request $request
     */
    public function addrole(Request $request)
    {
        $role = $request->post();
        $result = (new Ro())->addrole($role);
        return json($result);
    }


}
