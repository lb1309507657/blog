<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\facade\Log;
use app\admin\model\Admin;
use think\facade\Session;

/**
 * 登录
 * Class Login
 * @package app\admin\controller
 */
class Login
{
    // 登录
    public function login(Request $request)
    {
        if ($request->isAjax()) {  // 判断是否为ajax请求
            $admin = $request->post();  // 获取用户提交的数据
            // 实例化管理员模型
            $result = (new Admin())->check($admin);
            return json($result);
        } else {
            // 模板赋值和渲染
            return View::fetch();
        }
    }

    // 退出
    public function logout()
    {
        Session::delete('adminInfo');
        return ["code" => 1, "message"=>"退出成功"];
    }
}
