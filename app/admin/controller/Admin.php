<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use app\admin\controller\Base;
use app\admin\model\Role;
use think\facade\Log;
use app\admin\model\Admin as MAdmin;

/**
 * 管理员管理
 * Class Admin
 * @package app\admin\controller
 */
class Admin extends Base
{
    /**
     * 显示管理员列表列表
     *
     * @return \think\Response
     */
    public function index()
    {
        // 获取角色列表
        $roles = (new Role())->allrole();

        // 获取管理员列表
        $adminlist = (new MAdmin())->adminlist();

        // 模板赋值和渲染
        View::assign([
            "roles" => $roles,
            "adminlist" => $adminlist
        ]);
        return View::fetch();
    }

    // 添加管理员
    public function addadmin(Request $request)
    {
        $admin = $request->post();
        $result = (new MAdmin())->addadmin($admin);
        return json($result);
    }


}
