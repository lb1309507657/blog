<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\response\Redirect;
use think\facade\Log;
use think\facade\Db;
use app\admin\model\Article as MArt;
use app\admin\controller\Base;

/**
 * 文章管理
 * Class Article
 * @package app\admin\controller
 */
class Article extends Base
{
    // 文章列表
    public function artlist()
    {
        // 获取文章列表
        $artlist = (new MArt())->artlist();

        View::assign([
                "artlist" =>$artlist,
            ]
        );
        return View::fetch();
    }


    // 上传图片
    public function upimage(Request $request)
    {

        $file = $request->file("img");
        // 将图片上传到public
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'image', $file);
        return json(["path" => $savename]);
    }

    // 添加文章
    public function addarticle(Request $request)
    {
        $article = $request->post();
        $result = (new MArt())->addarticle($article);
        return json($result);
    }

    // 富文本编辑器的图片上传接口
    public function eupimage(Request $request)
    {
        try{
            $file = $request->file("file");
            // 将图片上传到public
            $savename = \think\facade\Filesystem::disk('public')->putFile( 'layui', $file);
            return json([
                    "code" => 0,
                    "msg" => "",
                    "data" => [
                        "src" => "/storage/".$savename,
                    ]
            ]);
        }catch(\Exception $e){
            return json([
                "code" => 1,
                "msg" => "图片上传失败",
            ]);
        }
    }
}
