<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\Request;
use think\facade\View;
use think\response\Redirect;
use think\facade\Log;
use app\admin\model\Category as MCate;
use app\admin\controller\Base;

class Category extends Base
{
    // 分类列表
    public function catelist()
    {
        $catelist = (new MCate())->catelist();

        View::assign([
            "catelist" => $catelist,
        ]);
        return View::fetch();
    }


    /**
     * 添加分类
     */
    public function addcate(Request $request)
    {
        // 获取分类数据
        $cate = $request->post();
        $result = (new MCate())->addcate($cate);
        return json($result);
    }
}
