<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\App;
use think\facade\Session;
use think\Request;
use app\BaseController;
use liliuwei\think\Jump;
use think\response\Redirect;
use think\facade\Db;

class Base extends BaseController
{
    use Jump;
    public function __construct(App $app)
    {
        parent::__construct($app);
        // 权限检测
        if (Session::has("adminInfo")) {
            $info = Session::get("adminInfo");
            // 判断管理员是否有权限执行当前请求
            $appname = $app->http->getName();  // 获取当前应用名称
            $controller = strtolower($app->request->controller());  // 获取当前控制器
            $action = $app->request->action();     // 获取当前方法
            // 如果是超级管理员或者是访问首页 则不判断
            if (!($info["username"] == "root" || ($appname == "admin" && $controller == "index" && $action=="index"))) {
                // 查询权限
                $res = Db::name("admin")->alias("a")
                    ->join("six_admin_role ar", "a.id = ar.admin_id")
                    ->join("six_role_privilege rp", "ar.role_id=rp.role_id")
                    ->join("six_privilege p", "rp.privilege_id=p.id")
                    ->where([
                        "a.id" =>$info["aid"],
                        "p.app_name" =>$appname,
                        "p.controller_name" =>$controller,
                        "p.action_name" =>$action,
                    ])->find();
                if (!$res) {
                    return $this->error("没有权限");
                }
            }
        } else {
            return $this->error("没有登陆",url("login/login")->build());
        }
    }

}
