<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use app\admin\validate\Module as Vmodule;
use think\facade\Log;

/**
 * 模块模型管理
 * @mixin think\Model
 */
class Module extends Model
{
    protected $autoWriteTimestamp = true;

    // 添加模块
    public function addmodule($module)
    {
        // 实例化验证器
        $vmodule =  new Vmodule();
        if ($vmodule->scene("add")->check($module)) {
            // 添加数据
            try{
                self::create($module);
                return ["code" => 1, "message" => "添加成功"];
            }catch(\Exception $e){
                Log::write($e->getMessage());
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else{
            return ["code" => 0, "message" => $vmodule->getError()];
        }
    }

    // 模块列表 返回所有的模块
    public function modulelist()
    {
        return self::select()->toArray();
    }

    // 修改模块
    public function updatemodule($module)
    {
        // 实例化验证器
        $vmodule =  new Vmodule();
        if ($vmodule->scene("edit")->check($module)) {
            // 添加数据
            try{
                self::update($module);
                return ["code" => 1, "message" => "修改成功"];
            }catch(\Exception $e){
                Log::write($e->getMessage());
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else{
            return ["code" => 0, "message" => $vmodule->getError()];
        }
    }

}
