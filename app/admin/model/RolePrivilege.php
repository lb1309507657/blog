<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\model\Pivot;

/**
 * @mixin think\Model
 */
class RolePrivilege extends Pivot
{
    //
}
