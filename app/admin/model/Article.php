<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use app\admin\validate\Article as VArt;
use think\facade\Session;
use think\facade\Log;

/**
 * @mixin think\Model
 */
class Article extends Model
{
    // 添加文章
    public function addarticle($article)
    {
        // 实例化控制器
        $vart = new VArt();
        if ($vart->scene("add")->check($article)) {
            // 通过session去获取用户的信息
            $userinfo = Session::get("adminInfo");
            $uid = $userinfo["aid"];
            $author = $userinfo["username"];
            $article["uid"] = 1;
            $article["author"] = "friends";
            unset($article["file"]);
            unset($article["null"]);
            unset($article["img"]);

            Log::write($article);


            // 添加分类
            try{
                self::create($article);
                return ["code" =>1, "message" => "添加成功"];
            }catch(\Exception $e){
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else {
            return ["code" =>0, "message" => $vart->getError()];
        }

    }

    // 获取文章列表
    public function artlist()
    {
        return self::select()->toArray();
    }
}
