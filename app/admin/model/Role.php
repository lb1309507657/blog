<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use think\facade\Db;

/**
 * @mixin think\Model
 */
class Role extends Model
{
    /**
     * 添加角色
     * @param $role 角色数据
     */
    public function addrole($role)
    {
        try{
            self::create(["role_name" =>$role["role_name"]])->privilege()->saveAll($role["pri_id"]);
            return ["code"=>1, "message" => "添加成功"];
        }catch(\Exception $e){
            return ["code"=>0, "message" => $e->getMessage()];
        }
    }

    // 定义关联方法
    public function privilege()
    {
        return $this->belongsToMany(Privilege::class,RolePrivilege::class,'privilege_id','role_id');
    }

    // 获取所有的角色
    public function rolelist()
    {
        //
        return self::table("six_role")->alias("r")->field("r.id,r.role_name,group_concat(p.pri_name) pri_name")
            ->join("six_role_privilege rp", "r.id=rp.role_id")
            ->join("six_privilege p", "rp.privilege_id=p.id")
            ->group("r.role_name")->select()->toArray();

        // 第二种实现方式
//        $sql = "
//                select sr.id,sr.role_name,group_concat(sp.pri_name) as pri_name from six_role as sr
//                left join six_role_privilege as srp
//                on sr.id = srp.role_id
//                left join six_privilege as sp
//                on srp.privilege_id = sp.id
//                where sr.id = 1 group by sr.role_name ;
//
//        ";
//        return Db::query($sql);

    }

    // 获取所有的角色(不包含权限)
     public function allrole()
     {
         return self::select()->toArray();
     }
}
