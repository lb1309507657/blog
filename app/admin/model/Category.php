<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use app\admin\validate\Category as VCate;
use think\model\concern\SoftDelete;

/**
 * @mixin think\Model
 */
class Category extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $autoWriteTimestamp = true;
    // 添加分类
    public function addcate($cate)
    {
        // 实例化控制器
        $vcate = new VCate();
        if ($vcate->scene("add")->check($cate)) {
            // 添加分类
            try{
                self::create($cate);
                return ["code" =>1, "message" => "添加成功"];
            }catch(\Exception $e){
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else {
            return ["code" =>0, "message" => $vcate->getError()];
        }

    }


    // 获取分类列表
    public function catelist()
    {
        return self::select()->toArray();
    }

}
