<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Log;
use think\Model;
use app\admin\validate\Login;
use think\facade\Config;
use think\facade\Session;
use app\admin\validate\Admin as VAdmin;
use app\admin\model\Role;
use app\admin\model\AdminRole;


/**
 * 管理员模型
 * @mixin think\Model
 */
class Admin extends Model
{
    /**
     * 用户登录检测
     * @param $user 用户数据
     */
    public function check($user)
    {
        // 1. 验证数据是否合法
        $vlogin = new Login(); // 实例化验证器
        if ($vlogin->check($user)) {
            // 2. 查询管理员数据库 判断是否存在这个用户
            $admin = self::where("username", $user["username"])->find();
            if (!$admin->isEmpty()) {
                // 3. 验证密码是否正确和账户是否启用
                if (md5(Config::get("cus.salt").$user["password"]) == $admin->password  && $admin->status == 1) {
                    // 4. 将用户信息写入session中
                    Session::set("adminInfo",["aid" =>$admin->id,"username" =>$admin->username]);
                    return ["code" => 1, "message" => "登录成功"];
                } else {
                    return ["code" => 0, "message" => "密码错误或者账户被禁用"];
                }
            } else {
                return ["code" => 0, "message" => "管理员不存在"];
            }
        } else {
            return ["code" => 0, "message" =>$vlogin->getError()];
        }

    }

    // 添加管理员
    public function addadmin($admin)
    {
        // 实例化验证器
        $vadmin = new VAdmin();
        if($vadmin->scene("add")->check($admin)){
            // 添加管理员
            if($admin["status"] == "on"){
                $admin["status"] = 1;
            } else {
                $admin["status"] = 0;
            }
            $data = [
                "username"   => $admin["username"],
                "password"   => md5(Config::get("cus.salt").$admin["password"]),
                "real_name"  =>$admin["username"],
                "add_time"   =>time(),
                "status"     => $admin["status"],
            ];
            // 添加数据
            try{
                self::create($data)->roles()->saveAll($admin["role_id"]);
                return ["code"=>1, "message" => "添加成功"];
            }catch(\Exception $e){
                return ["code"=>0, "message" => $e->getMessage()];
            }
        } else {
            return ["code" => 0, "message" => $vadmin->getError()];
        }
    }

    // 定义关联角色的关联方法
    public function roles()
    {
        return $this->belongsToMany(Role::class,AdminRole::class,'role_id','admin_id');
    }

    // 获取管理员列表(包含角色)
    public function adminlist()
    {
        return self::table("six_admin")->alias("a")
            ->field("a.id,a.username,group_concat(r.role_name) role_name")
            ->leftJoin("six_admin_role ar", "a.id=ar.admin_id")
            ->leftJoin("six_role r", "ar.role_id=r.id")
            ->group("a.username")->order("a.id asc")->select()->toArray();
    }
}
