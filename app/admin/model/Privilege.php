<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use app\admin\validate\Privilege as Vpri;
use think\facade\Log;
use think\model\concern\SoftDelete;

/*
 * 权限模型
 * @mixin think\Model
 */
class Privilege extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';

    /**
     * 添加权限
     * @param $pri  权限数据
     */
    public function savepri($pri)
    {
        // 实例化权限验证器
        $vpri =new Vpri();
        if ($vpri->scene("add")->check($pri)) {
            // 添加数据
            self::create($pri);
            return ["code" => 1, "message" => "添加成功"];
        } else {
            return ["code" => 0, "message" =>$vpri->getError()];
        }
    }

    // 获取所有权限
    public function listpri()
    {
        return self::select()->toArray();
    }

    // 修改权限
    public function editpri($pri)
    {
        // 实例化权限验证器
        $vpri =new Vpri();

        if ($vpri->scene("edit")->check($pri)) {
            // 修改数据
            try{
                self::update(["jack"=>"asdfasd"]);
                return ["code" => 1, "message" => "更新成功"];
            }catch(\Exception $e){
                return ["code" =>0, "message" => $e->getMessage()];
            }

            return ["code" => 1, "message" => "更新成功"];
        } else {
            return ["code" => 0, "message" => $vpri->getError()];
        }
    }

    /**
     * 删除权限
     * @param $pid 权限的id
     */
    public function delpri($pid)
    {
        try{
            self::destroy($pid);
            return ["code" => 1, "message" => "删除成功"];
        } catch(\Exception $e){
            return ["code" => 0, "message" => $e->getMessage()];
        }
    }
}
