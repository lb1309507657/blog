<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\Model;
use app\admin\validate\Set;
use think\facade\Log;

/**
 * 网站设置选项
 * @mixin think\Model
 */
class Options extends Model
{
    /**
     * 添加基本的网站设置选项
    * @param $base  选项数据
     */
    public function addbase($base)
    {
        // 实例化验证器
       $vset =  new set();
       if ($vset->scene("base")->check($base)) {
           // 添加数据
           $newbase = [];
           foreach ($base as $key => $value) {
                $tmp=[];
                $tmp["name"] = $key;
                $tmp["value"] = $value;
                $tmp["type"] = "base";
                $newbase[] = $tmp;
           }
           // 批量添加
           try{
               $this->saveAll($newbase);
               return ["code" => 1, "message" => "添加成功"];
           }catch(\Exception $e){
               return ["code" =>0, "message" => $e->getMessage()];
           }
       } else{
           return ["code" => 0, "message" => $vset->getError()];
       }
    }

    /**
     * 添加注册选项
     * @param $reg
     */
    public function addreg($reg)
    {
        // 实例化验证器
        $vset =  new set();
        if ($vset->scene("reg")->check($reg)) {
            // 添加数据
            $newreg = [];
            foreach ($reg as $key => $value) {
                $tmp=[];
                $tmp["name"] = $key;
                $tmp["value"] = $value;
                $tmp["type"] = "reg";
                $newreg[] = $tmp;
            }
            // 批量添加
            try{
                $this->saveAll($newreg);
                return ["code" => 1, "message" => "添加成功"];
            }catch(\Exception $e){
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else{
            return ["code" => 0, "message" => $vset->getError()];
        }
    }

    /**
     * 添加主题
     * @param $theme
     */
    public function addtheme($theme)
    {
        // 添加数据
        $newtheme = [];
        foreach ($theme as $key => $value) {
            $tmp = [];
            $tmp["name"] = $key;
            $tmp["value"] = $value;
            $tmp["type"] = "theme";
            $newtheme[] = $tmp;
        }
        // 批量添加
        try{
            $this->saveAll($newtheme);
            return ["code" =>1, "message"=> "添加成功"];
        }catch(\Exception $e){
            return ["code" =>0, "message"=>$e->getMessage()];
        }
    }

    /**
     * 添加邮箱数据
     * @param $email 邮箱数据
     */
    public function addemail($email)
    {

        // 实例化验证器
        $vset = new set();
        if ($vset->scene("email")->check($email)) {
            // 添加数据
            $type = $email["type"];
            unset($email["type"]);
            $newemail = [];
            foreach ($email as $key => $value) {
                $tmp = [];
                $tmp["name"] = $key;
                $tmp["value"] = $value;
                $tmp["type"] = $type;
                $newemail[] = $tmp;
            }
            // 批量添加
            try{
                $this->saveAll($newemail);
                return ["code" =>1, "message"=> "添加成功"];
            }catch(\Exception $e){
                return ["code" =>0, "message"=>$e->getMessage()];
            }
        }
    }

    // 获取邮箱的选项
    public function getmail()
    {
        return self::where("type", "mailBase")->select()->toArray();
    }

}
