<?php
declare (strict_types = 1);

namespace app\home\model;

use think\Model;
use app\home\validate\Reg;
use think\facade\Config;
use think\facade\Session;
use app\home\validate\Login;
use think\facade\Db;

/**
 * 用户管理
 * @mixin think\Model
 */
class User extends Model
{
    // 添加用户
    public function adduser($user)
    {
        // 实例化验证器
        $vreg = new Reg();
        if ($vreg->check($user)) {
            // 添加用户
            $udata = [
                "username" => $user["username"],
                "password" => md5(Config::get("cus.salt").$user["password"]),
                "email" => $user["email"],
                "image" => "/static/images/user_defaule.png",
                "add_time" => time(),
            ];
            try{
                $userobj = self::create($udata);
                Session::set("uid", $userobj->id);
                Session::set("uname", $userobj->username);
                return ["code" => 1, "message" => "注册成功"];
            }catch(\Exception $e){
                return ["code" => 0, "message" => $e->getMessage()];
            }
        } else {
            return ["code" => 0, "message" => $vreg->getError()];
        }

    }

    // 根据用户的id 获取用户信息
    public function getuser($id)
    {
        return self::where("id",$id)->find()->toArray();

    }


    // 用户登陆
    public function login($login)
    {
        // 实例化验证器
        $vlogin = new Login();
        if ($vlogin->check($login)) {
            //

            // 2. 查询管理员数据库 判断是否存在这个用户
            $res = self::where("email", $login["email"])->find();
            if ($res) {
                // 3. 验证密码是否正确和账户是否启用
                if (md5(Config::get("cus.salt").$login["password"]) == $res["password"]) {
                    // 4. 将用户信息写入session中
                    Session::set("uid",$res["id"]);
                    Session::set("uname",$res["username"]);
                    // 修改用户最后一次登陆的时间
                    Db::name("user")->where("id",$res["id"])->update(["last_time"=>time()]);
                    return ["code" => 1, "message" => "登录成功"];
                } else {
                    return ["code" => 0, "message" => "密码错误"];
                }
            } else {
                return ["code" => 0, "message" => "用户不存在"];
            }

        } else {
            return ["code" => 0, "message" => $vlogin->getError()];
        }

    }
}
