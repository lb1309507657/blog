<?php
declare (strict_types = 1);

namespace app\home\model;

use think\Model;
use app\home\validate\Topic as VTopic;
use think\facade\Session;
use think\model\concern\SoftDelete;
use think\facade\Db;

/**
 * 主题管理
 * @mixin think\Model
 */
class Topic extends Model
{
    use SoftDelete;
    protected $deleteTime = 'delete_time';
    protected $autoWriteTimestamp = true;

    // 添加主题
    public function addtopic($topic)
    {
        // 实例化验证器
        $vtopic = new VTopic();
        if ($vtopic->check($topic)) {
            unset($topic["null"]);
            $uid = Session::get("uid");
            $topic["uid"] = $uid;
            // 添加数据
            try{
                self::create($topic);
                // 修改用户的主题数
                Db::name("user")->where("id",$uid)->inc("topics")->update();
                return ["code" =>1, "message" => "发布成功，正在跳转....."];
            }catch(\Exception $e){
                return ["code" =>0, "message" => $e->getMessage()];
            }
        } else {
            return ["code" => 0, "message" => $vtopic->getError()];
        }
    }

    // 根据条件来获取主题
    public function getTopic($where)
    {
        return self::table("six_topic")->alias("t")
            ->field("t.id,t.title,t.content,t.comments,t.view,t.likes,t.create_time,t.top,t.essence,t.update_time,u.id as uid,u.username,u.image,u.topics,u.comments,u.essences,m.name,m.id as mid")
            ->join("six_user u","t.uid=u.id")
            ->join("six_module m", "t.mid=m.id")
            ->where($where)
            ->select()->toArray();
    }


    // 获取分页的主题
    public function getPageTopic($where,$page,$limit)
    {
        return self::table("six_topic")->alias("t")
            ->field("t.id,t.title,t.content,t.comments,t.view,t.likes,t.create_time,t.top,t.essence,t.update_time,u.id as uid,u.username,u.image,u.topics,u.comments,u.essences,m.name,m.id as mid")
            ->join("six_user u","t.uid=u.id")
            ->join("six_module m", "t.mid=m.id")
            ->page($page,$limit)
            ->where($where)
            ->select()->toArray();

    }
}
