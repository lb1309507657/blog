<?php
declare (strict_types = 1);

namespace app\home\model;

use think\facade\Db;
use think\Model;

/**
 * @mixin think\Model
 */
class Comment extends Model
{
    // 根据帖子的id来获取评论
    public function getcomment($tid)
    {
       return self::table("six_comment")->alias("c")
               ->field("c.id,c.content,c.create_time,u.id as uid,u.username,u.image")
           ->join("six_user u", "c.uid=u.id")
           ->where("tid",$tid)->select()->toArray();
    }

    // 添加评论
    public function addcomment($comment)
    {

        try{
            self::create($comment);
            Db::name("user")->where("id",$comment["uid"])->inc("comments")->update();
            return ["code" => 1, "message"=>"评论成功"];
        }catch(\Exception $e){
            return ["code" => 0, "message" => "评论失败"];
    }


    }
}
