<?php
declare (strict_types = 1);

namespace app\home\model;

use think\Model;
use think\facade\Log;

/**
 * @mixin think\Model
 */
class Appendix extends Model
{
    // 添加附件
    public function addappendix($appendix)
    {
        try{
            self::create($appendix);
            return ["code" => 1, "message"=> "添加成功"];
        }catch(\Exception $e){
            Log::write("附件上传异常".$e->getMessage());
            return ["code" => 0, "message" => $e->getMessage()];
        }

    }
}
