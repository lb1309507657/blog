<?php
declare (strict_types = 1);

namespace app\home\model;

use think\facade\Db;
use think\Model;

/**
 * 文章管理
 * @mixin think\Model
 */
class Article extends Model
{

    // 文章列表
    public function artlist($where,$page, $limit)
    {
        return self::table("six_article")->alias("a")
            ->field("a.id,a.title,a.content,a.views as view,a.comments,a.likes,a.create_time,a.image,m.username,c.category_name as name")
            ->join("six_admin m","a.uid=m.id")
            ->join("six_category c","a.cid=c.id")
            ->where($where)
            ->page($page,$limit)
            ->select()->toArray();
    }

    // 根据文章的id来获取文章的内容
    public function getarticle($id)
    {
        return self::table("six_article")->alias("a")
            ->field("a.id,a.title,a.content,a.views as view,a.comments,a.likes,a.create_time,a.update_time,a.image,m.username,m.id as uid,c.category_name as name")
            ->join("six_admin m","a.uid=m.id")
            ->join("six_category c","a.cid=c.id")
            ->where(["a.id"=>$id])
            ->find()->toArray();
    }



}
