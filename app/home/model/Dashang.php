<?php
declare (strict_types = 1);

namespace app\home\model;

use think\Model;
use app\home\validate\Dashang as VDashang;
use think\facade\Session;

/**
 * @mixin think\Model
 */
class Dashang extends Model
{
    // 添加打赏
    public function dashang($info)
    {
        // 实例化验证器
        $vmds = new VDashang();
        if ($vmds->check($info)) {
            $data = [
                "uid" => Session::get("uid"),
                "aid" => $info["article"],
                "account" => $info["account"],
                "message" => $info["message"],
                "paystatus" => 0,
                "create_time" => time(),
                "orderNo" => $info["orderNo"],
            ];
            // 写入打赏表
            try{
                self::create($data);
                return ["status" => true, "message" => "添加成功"];
            }catch(\Exception $e){
                return ["status" => false, "message"=>$e->getMessage()];
            }

        } else {
            return ["status" =>false,"message" => $vmds->getError()];
        }


    }
}
