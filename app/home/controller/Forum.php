<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use think\facade\View;
use think\facade\Session;

/**
 * 模块帖子管理
 * Class Forum
 * @package app\home\controller
 */
class Forum
{
    // 显示对应模块下的帖子
    public function index(Request $request)
    {
        $mid = $request->param("mid");
        if (Session::has("uid")) {
            $userData = (new \app\home\model\User())->getuser(Session::get("uid"));
        }

        View::assign([
            "fid" => $mid,
            "userData" =>isset($userData)?$userData:"",
        ]);
        return View::fetch();
    }
}
