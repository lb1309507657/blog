<?php
declare (strict_types = 1);

namespace app\home\controller;

use app\home\model\User;
use think\facade\Session;
use think\Request;
use think\facade\View;
use app\admin\model\Module;
use think\facade\Log;
use liliuwei\think\Jump;
use app\BaseController;
use app\home\model\Topic as MTopic;
use app\home\model\Article;
use think\facade\Db;


/**
 * 主题管理
 * Class Topic
 * @package app\home\controller
 */
class Topic extends BaseController
{
    use Jump;
    // 发帖
    public function create(Request $request)
    {
        if ($request->isAjax()) {
            $topic = $request->post();
            $res  = (new MTopic())->addtopic($topic);
            return json($res);
        }

        // 判断用户是否登陆
        if (Session::has("uid")) {
            $userData = (new User())->getuser(Session::get("uid"));
        } else {
            return $this->error("没有登陆", "/");
        }

        // 获取模块列表
        $mlist = (new Module())->modulelist();

        // 渲染
        View::assign([
            "userData" => $userData,
            "mlist" => $mlist,
            "sign" => \createstr(30)
        ]);
        return View::fetch();
    }

    // 显示帖子的详情
    public function index(Request $request)
    {
        if (Session::has("uid")) {
            $userData = (new User())->getuser(Session::get("uid"));
        }

        $param = $request->param();
        if ($param["mid"] == "undefined") {
            $aid = $param["tid"];
            $topicData = (new Article())->getarticle($aid);
        } else {
            // 获取到帖子的id
            $tid = $param["tid"];
            $topicData = ((new MTopic())->getTopic(["t.id"=>$tid]))[0];
        }

        // 赋值和渲染
        View::assign([
            "topicData" => $topicData,
            "userData" => isset($userData)?$userData:"",
        ]);
        return View::fetch();
    }


    // 获取帖子列表
    public function topiclist(Request $request)
    {
        $param = $request->param();

        if ($param["fid"] == "4") {
            $artlist = (new Article())->artlist([],(int)$param["page"], 1);
            return json(["data"=>$artlist]);
        } else {

            if ($param["t"] == "1") {  // 综合
                if ($param["fid"]) {
                    $topiclist = (new MTopic())->getPageTopic(["m.id"=>$param["fid"]],(int)$param["page"],2);
                } else{
                    $topiclist = (new MTopic())->getPageTopic([],(int)$param["page"],2);
                }
                return json(["data"=>$topiclist]);
            } else {  // 精华
                if ($param["fid"]) {
                    $topiclist = (new MTopic())->getPageTopic(["t.essence"=>1,"m.id"=>$param["fid"]],(int)$param["page"],2);
                } else {
                    $topiclist = (new MTopic())->getPageTopic(["t.essence"=>1],(int)$param["page"],2);
                }
                return json(["data"=>$topiclist]);
            }
        }
    }
}
