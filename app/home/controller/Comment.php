<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use app\home\model\Comment as MComment;
use think\facade\Session;

/**
 * 评论管理
 * Class Comment
 * @package app\home\controller
 */
class Comment
{
    // 获取评论列表
    public function commentlist(Request $request)
    {
        // 获取帖子id
        $tid = $request->param("tid");
        $res = (new MComment())->getcomment($tid);
        return json(["data" =>$res]);

    }


    // 添加评论
    public function comment(Request $request)
    {
        if (Session::has("uid")) {

            $recid = $request->post("reCid");
            $recid = isset($recid)? $recid:"0";


            // 获取主题的id
            $tid = $request->param("tid");
            // 获取评论的内容
            $content = $request->post("content");

            // 构建评论数据
            $comment = [
                "tid" => $tid,
                "uid" =>Session::get("uid"),
                "content" => $content,
                "recid" =>$recid,
            ];

            $res = (new MComment())->addcomment($comment);
            return json($res);
        } else {
            return json(["code" => 0, "message" => "没有登陆"]);
        }



    }

}
