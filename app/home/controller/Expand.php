<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use app\home\model\Appendix;

/**
 * 附件上传
 * Class Expand
 * @package app\home\controller
 */
class Expand
{
    // 上传附件
    public function upload(Request $request)
    {
        // 获取表单上传文件 例如上传了001.jpg
        $file = $request->file('file');
        // 路径
        $savename = \think\facade\Filesystem::putFile( 'appendix', $file);
        // 文件的名称
        $filename = $file->md5();

        $info = $request->post();
        $uid = $info["uid"];
        $sign = $info["sign"];

        $res = (new Appendix())->addappendix([
            "uid" => $uid,
            "sign" => $sign,
            "filename" => $filename,
            "path" => $savename
        ]);

        return json($res);
    }
}
