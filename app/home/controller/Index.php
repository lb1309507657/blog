<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use think\facade\View;
use think\facade\Session;
use app\home\model\User;
use app\home\model\Topic;

/**
 * 前台首页
 * Class Index
 * @package app\home\controller
 */
class Index
{
    // 前台首页
    public function index()
    {
        // 判断用户是否登陆
        if (Session::has("uid")) {
            $userData = (new User())->getuser(Session::get("uid"));
        }

        // 获取置顶内容
        $tops = (new Topic())->getTopic(["t.top" => 1]);

        // 渲染页面
        View::assign([
            "userData" => isset($userData)?$userData:"",
            "tops" => $tops
        ]);
        return View::fetch();
    }

}
