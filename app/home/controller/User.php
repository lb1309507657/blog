<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use think\facade\View;
use think\facade\Log;
use think\facade\Db;
use app\home\controller\Mail;
use app\home\model\User AS MUser;
use think\facade\Session;
use app\home\model\Topic;

/**
 * 用户管理
 * Class User
 * @package app\home\controller
 */
class User
{
    // 注册
    public function reg(Request $request)
    {
        if ($request->isAjax()) {
            $reg = $request->post();
            $result = (new MUser())->adduser($reg);
            return json($result);
        }
        // 渲染页面
        return View::fetch();
    }

    // 发送邮件
    public function sendmail(Request $request)
    {
        $info = $request->post();
        $username = $info["username"]; // 用户名称
        $mail = $info["email"];
        // 判断邮箱是否已经注册
        $res = Db::name("user")->where("mail",$mail)->find();
        if ($res) {
            return ["code"=>0, "message" => "邮箱已注册"];
        }

        // 生成邮箱验证码
        $code = \createstr(6);

        $mailobj = new Mail();

        $content = "尊敬的用户你好：你的邮箱验证码【 $code 】";
        $res = $mailobj->send($mail, $username, "邮箱验证", $content);

        if ($res) {
            session('regCode', $code);
            return json(['code' => '1', 'message' => '发送成功！']);
        } else {
            return json(['code'=>'0','message'=>$mailobj->errorMsg]);
        }
    }

    // 退出功能
    public function logout()
    {
        // 删除session里面的uid
        Session::delete("uid");
        return redirect("/");
    }

    // 用户登陆
    public function login(Request $request)
    {
        if ($request->isAjax()) {
            $login = $request->post();
            $res = (new MUser())->login($login);
            return json($res);
        }

        return View::fetch();
    }


    // 用户的个人信息展示
    public function index()
    {

        $userData = (new \app\home\model\User())->getuser(Session::get("uid"));

        // 获取发帖人总共发的主题
        $userTopic = (new Topic())->getTopic(["uid"=>Session::get("uid")]);

        View::assign([
            "userData" => $userData,
            "userTopic" => $userTopic,
        ]);
        return View::fetch();
    }

    // 发帖人信息
    public function tauthorinfo(Request $request)
    {
        // 获取发帖人的信息
        $uid = $request->param("uid");
        $userdata = (new MUser())->getuser($uid);


        // 获取发帖人总共发的主题
        $userTopic = (new Topic())->getTopic(["uid"=>$uid]);

        View::assign([
            "userInfo" => $userdata,
            "userTopic" => $userTopic,
        ]);
        return View::fetch();
    }


    // 修改用户的头像
    public function updateimage(Request $request)
    {
        // 获取表单上传文件 例如上传了001.jpg
        $file = $request->file('avatar');
        // 路径
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'user', $file);

        $savename = "/storage/".$savename;
        Db::name("user")->where("id",Session::get("uid"))->update(["image" =>"".$savename]);
        return json(["code"=>1, "url"=>$savename]);
    }

}
