<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use PHPMailer\PHPMailer\PHPMailer;
use app\admin\model\Options;
use think\facade\Log;

/**
 * 邮件发送
 * Class Mail
 * @package app\home\controller
 */
class Mail
{
    public $fromName;
    public $fromAdress;
    public $smtpHost;
    public $smtpPort;
    public $replyTo;
    public $smtpUser;
    public $smtpPass;
    public $encriptionType;
    public $errorMsg;

    public function __construct(){
        $options =  (new Options())->getmail();

        $mailOptions = [];
        foreach ($options as $option) {
            $mailOptions[$option["name"]] = $option["value"];
        }
        $this->fromName = $mailOptions["fromName"];
        $this->fromAdress = $mailOptions["fromAdress"];
        $this->smtpHost = $mailOptions["smtpHost"];
        $this->smtpPort = $mailOptions["smtpPort"];
        $this->replyTo = $mailOptions["replyTo"];
        $this->smtpUser = $mailOptions["smtpUser"];
        $this->smtpPass = $mailOptions["smtpPass"];
        $this->encriptionType = $mailOptions["encriptionType"];
    }

    public function Send($to,$name,$title,$content){
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPAuth=true;
        $mail->Host = $this->smtpHost;
        if(!empty($this->encriptionType) && $this->encriptionType != "no"){
            $mail->SMTPSecure = $this->encriptionType;
        }
        $mail->Port = $this->smtpPort;
        $mail->CharSet = 'UTF-8';
        $mail->FromName = $this->fromName;
        $mail->Username =$this->smtpUser;
        $mail->Password = $this->smtpPass;
        $mail->From = $this->fromAdress;
        $mail->isHTML(true);
        $mail->addAddress($to,$name);
        $mail->Subject = $title;
        $mail->Body = $content;
        $status = $mail->send();
        if(!$status){
            $this->errorMsg =  $mail->ErrorInfo;
            return false;
        }
        return true;
    }


}
