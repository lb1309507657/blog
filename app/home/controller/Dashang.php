<?php
declare (strict_types = 1);

namespace app\home\controller;

use think\Request;
use think\facade\Session;
use app\home\model\Dashang as MDashang;
use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;
use think\facade\Config as SConfig;
use think\facade\Log;
use think\facade\Db;

/**
 * 打赏功能
 * Class Dashang
 * @package app\home\controller
 */
class Dashang
{
    // 打赏
    public function dashang(Request $request)
    {
        if (Session::has("uid")) {
            $info = $request->post();
            $orderNo = time() . rand(1000, 9999);
            $info["orderNo"] = $orderNo;
            // 实例化模型
            $result = (new MDashang())->dashang($info);
            if ($result["status"]) {
                // 条状到支付的页面
                if ($info["pay"] == "alipay") { // 判断是支付宝还是微信
                    // 获取支付宝的配置信息
                    $aliConfig =SConfig::get("aliconfig");
                    // 构建支付信息
                    $payData = [
                        'body'    => '六星教育',
                        'subject'    => '文章打赏',
                        'order_no'    => $orderNo,
                        'timeout_express' => time() + 600,// 表示必须 600s 内付款
                        'amount'    => $info["account"],// 单位为元 ,最小为0.01
                        // 'client_ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',// 客户地址
                        'goods_type' => '1',// 0—虚拟类商品，1—实物类商品
                        'store_id' => '',
                        // 说明地址：https://doc.open.alipay.com/doc2/detail.htm?treeId=270&articleId=105901&docType=1
                        // 建议什么也不填
                        'qr_mod' => '',
                    ];
                    // 阿里支付
                    try {
                        $url = Charge::run(Config::ALI_CHANNEL_WEB, $aliConfig, $payData);
                    } catch (PayException $e) {
                        return $e->errorMessage();
                        exit;
                    }
                    return redirect($url);

                } else {
                    // 微信支付
                }

            } else {
                return $result["message"];
            }
        }

        // 没有登陆就跳转到登陆页面
        return redirect(url("user/login")->build());

    }

    // 支付宝 回调处理
    public function notify()
    {
        $data = file_get_contents("php://input");
        parse_str($data,$arr);
        if ($arr["trade_status"] == "TRADE_SUCCESS") {
            Db::name("dashang")->where("orderNo",$arr["out_trade_no"])->update(["paystatus"=>1]);
            return "success";
        } else {
            return "failure";
        }
    }
}
