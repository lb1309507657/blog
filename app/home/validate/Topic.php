<?php
declare (strict_types = 1);

namespace app\home\validate;

use think\Validate;
use liliuwei\think\Jump;

class Topic extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "title" => "require",
        "content" => "require"
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "title.require" => "标题不能为空",
        "title.content" => "内容不能为空",
    ];
}
