<?php
declare (strict_types = 1);

namespace app\home\validate;

use think\Validate;

class Dashang extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "account"=> "require",
        "message" => "require"
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "account.require" => "金额不能为空",
        "message.require" => "留言不能为空",
    ];
}
