<?php
declare (strict_types = 1);

namespace app\home\validate;

use think\Validate;

class Login extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "email" => "email",
        "password" => "require",
        "captcha" => "require|captcha",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "email.email" => "邮箱规则不符合",
        "password" => "密码不能为空",
        "captcha.captcha" => "验证码错误",
    ];
}
