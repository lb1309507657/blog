<?php
declare (strict_types = 1);

namespace app\home\validate;

use think\facade\Db;
use think\facade\Log;
use think\facade\Session;
use think\Validate;

class User extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
        "oldpassword"=>"require",
        "password"=>"require",
        "repassword"=>"require|confirm:password",
    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "oldpassword.require"=>"旧密码不能为空",
        "password.require"=>"新密码不能为空",
        "repassword.require"=>"确认新密码不能为空",
        "repassword.confirm"=>"确认新密码与新密码不一致",
    ];

    // 自定义验证规则
//    protected function checkName($value, $rule, $data=[])
//    {
//        $value = Db::table("six_user")->where("id",Session::get("uid"))->select();
//        $value = $value[0]["password"];
//        return $rule == $value ? true : '旧密码错误';
//    }
}
