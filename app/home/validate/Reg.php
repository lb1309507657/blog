<?php
declare (strict_types = 1);

namespace app\home\validate;

use think\Validate;
use think\facade\Session;

class Reg extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */	
	protected $rule = [
	    "email"   => "email",
        "username" => "require",
        "password" => "require",
        "repassword" => "require|confirm:password",
//        "code" => "checkcode",
        "captcha" => "require|captcha",

    ];
    
    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */	
    protected $message = [
        "email.email" => "邮箱不符合规范",
        "username.require" => "用户名不能为空",
        "password.require" => "密码不能为空",
        "repassword.require" => "确认密码不能为空",
        "repassword.confirm" => "密码必须等于确认密码",
        "captcha.captcha" => "验证码错误",
    ];


    //自定义验证验证邮箱验证码的方法
    public function checkcode($value, $rule, $data=[])
    {
        return $value == Session::get("regCode")?true:"邮箱验证码错误";
    }
}
