<?php
// 应用公共文件


// 生成有层级的数据
use think\facade\Db;

function make_tree($data,$parent_id = 0,$level = 0)
{
    static $newData = [];
    foreach ($data as $d) {
        if ($d["parent_id"] == $parent_id) {
            $d["level"] = $level;
            $newData[] = $d;
            make_tree($data,$d["id"],$level+1);
        }
    }
    return $newData;
}

// 获取版块
function outTopbar()
{
    $data = Db::name('module')->select();
    $html = '';
    foreach ($data as $key => $value) {
        $html .= '<a href="'.url('home/forum/index', ['mid'=>$value['id']]).'" class="mdui-hidden-xs" title="'.$value['name'].'">'.$value['name'].'</a>';
    }
    return $html;
}

// 运行时间
function get_runtime()
{
    return number_format(microtime(true)-\think\facade\App::getBeginTime(), 6, '.', '');
}


// 邮箱验证码
function createstr($length)
{
    $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符
    $strlen = 62;
    while ($length > $strlen) {
        $str .= $str;
        $strlen += 62;
    }
    $str = str_shuffle($str);
    return substr($str, 0, $length);
}


// 显示置顶和精华的标志
function outBadge($data)
{
    $value = '';

    if (isset($data["top"]) or isset($data["essence"])) {
        if ($data['top'] == 1) {
            $value = '<span class="mf-badge mf-badge-danger">置顶</span>';
        }
        if ($data['essence'] == 1) {
            $value = $value.'<span class="mf-badge mf-badge-warning">精华</span>';
        }
    }

    return $value;
}


