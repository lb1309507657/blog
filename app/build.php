<?php
/**
 * Created by PhpStorm.
 * User: mayn
 * Date: 2019/11/12
 * Time: 20:59
 */


return [
    // 需要自动创建的文件
    '__file__'   => [],
    // 需要自动创建的目录
    '__dir__'    => ['controller', 'model'],
    // 需要自动创建的控制器
    'controller' => [],
    // 需要自动创建的模型
    'model'      => [],
    // 需要自动创建的模板
//    'view'       => ['index/index'],
];