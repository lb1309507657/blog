/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : six_forum

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 21/04/2020 20:55:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for six_admin
-- ----------------------------
DROP TABLE IF EXISTS `six_admin`;
CREATE TABLE `six_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员名称',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `real_name` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '真实姓名',
  `role` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '角色',
  `last_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '127.0.0.1' COMMENT '最后一次登录的IP',
  `last_time` int(10) UNSIGNED NOT NULL DEFAULT 1573567992 COMMENT '最后一次登录的时间',
  `add_time` int(10) UNSIGNED NOT NULL COMMENT '创建账户的时间',
  `login_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户登录的次数',
  `status` int(10) UNSIGNED NOT NULL COMMENT '账户的状态 1可用 0禁用',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_admin
-- ----------------------------
INSERT INTO `six_admin` VALUES (1, 'admin', 'edb1cd41cbd3e8433c0acd0e4c1ea734', 'friendship', '0', '127.0.0.1', 1573567992, 1573567992, 0, 1);
INSERT INTO `six_admin` VALUES (2, 'jake-ma', 'edb1cd41cbd3e8433c0acd0e4c1ea734', 'jake-ma', '0', '127.0.0.1', 1573567992, 1584258610, 0, 1);

-- ----------------------------
-- Table structure for six_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `six_admin_role`;
CREATE TABLE `six_admin_role`  (
  `admin_id` int(10) UNSIGNED NOT NULL COMMENT '管理员id',
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色id',
  INDEX `admin_id`(`admin_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_admin_role
-- ----------------------------
INSERT INTO `six_admin_role` VALUES (2, 24);
INSERT INTO `six_admin_role` VALUES (2, 25);
INSERT INTO `six_admin_role` VALUES (2, 24);

-- ----------------------------
-- Table structure for six_appendix
-- ----------------------------
DROP TABLE IF EXISTS `six_appendix`;
CREATE TABLE `six_appendix`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户的id',
  `sign` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件的标识',
  `path` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件的路径',
  `filename` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '附件的名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '附件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_appendix
-- ----------------------------
INSERT INTO `six_appendix` VALUES (1, 1, 'RmaMYvl5iV2e01Uu3T4Q9C7bDkPFBX', 'appendix/20200404\\3f628a662b2770b1788b68eafe47a1be.txt', '4ee9f71b42787e3a9a2dd03cceb7d091');
INSERT INTO `six_appendix` VALUES (2, 1, 'RmaMYvl5iV2e01Uu3T4Q9C7bDkPFBX', 'appendix/20200404\\542b00d12819e6f492332c5c0d3ac8ec.txt', '4ee9f71b42787e3a9a2dd03cceb7d091');
INSERT INTO `six_appendix` VALUES (3, 1, 'RmaMYvl5iV2e01Uu3T4Q9C7bDkPFBX', 'appendix/20200404\\01427364c7bc5ffe00d484876db28650.txt', '4ee9f71b42787e3a9a2dd03cceb7d091');
INSERT INTO `six_appendix` VALUES (4, 1, 'RmaMYvl5iV2e01Uu3T4Q9C7bDkPFBX', 'appendix/20200404\\f336c9293794cd2e98ba3c07919f2d00.txt', '4ee9f71b42787e3a9a2dd03cceb7d091');
INSERT INTO `six_appendix` VALUES (5, 1, 'nS1voK6TmFHG3ae7IP50Xs4btxCZuD', 'appendix/20200404\\f3e2b67e9ecf5b7e281f0464c37104a7.txt', '4ee9f71b42787e3a9a2dd03cceb7d091');
INSERT INTO `six_appendix` VALUES (6, 1, 'QNF7G4jsTDxe8oadAYEkyq3bm1htWS', 'appendix/20200404\\20eaeff932ea79bae0c0537920242017.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (7, 1, 'QNF7G4jsTDxe8oadAYEkyq3bm1htWS', 'appendix/20200404\\7374bedce04701cca8fc8e12a3fb3b03.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (8, 1, 'CWx2NJ8oUv5zESRgan64hLqI9ZtMAw', 'appendix/20200404\\2f1ed098128379b208ff68861db5b836.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (9, 1, 'GW6ZqtlaSi5epjs31A8bcVwQBvfCTd', 'appendix/20200404\\2d8a651c9170b5801eda15d32a351df9.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (10, 1, 'paMS3JeNi8hgloRZ6HFVv72tbzAGcm', 'appendix/20200404\\683b427b7040d39f312fd1baa3cc597c.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (11, 1, 'jswf9qHhB4RnxkONKuEGy2UgAlr7eZ', 'appendix/20200404\\5ba38d4379bbcb136b60516f40cee337.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (12, 1, 'nc56gyO7HIJ2FGDApRWkd08M1UoLaK', 'appendix/20200404\\83d20c020c2f1c9d513e300c3a4d53d7.png', '6aa2c36f7ae51e4fe77b06bf4833cd71');
INSERT INTO `six_appendix` VALUES (13, 1, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 'appendix/20200405\\4e2284618b63b91419a7276f0cf38190.png', '631e78fab94c73bce16deb75e7b5cb08');
INSERT INTO `six_appendix` VALUES (14, 1, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 'appendix/20200405\\4e2284618b63b91419a7276f0cf38190.png', '631e78fab94c73bce16deb75e7b5cb08');
INSERT INTO `six_appendix` VALUES (15, 1, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 'appendix/20200405\\10b31a5c6ce0886e77d05b0a6732ceef.png', '631e78fab94c73bce16deb75e7b5cb08');
INSERT INTO `six_appendix` VALUES (16, 1, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 'appendix/20200405\\c5be431535aac4d3fd76a19917e7d5a0.png', '631e78fab94c73bce16deb75e7b5cb08');
INSERT INTO `six_appendix` VALUES (17, 1, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 'appendix/20200405\\b0ff95f924de089adfa118472b54a703.png', '631e78fab94c73bce16deb75e7b5cb08');
INSERT INTO `six_appendix` VALUES (18, 1, 'ZIkUQhTbHzS01yfYvsgwqJW3ax2E9D', 'appendix/20200407\\27cd426dbac687e5d3c02ccf8db81e02.png', '836a894858b9085f21b2cce9af4fe3b8');

-- ----------------------------
-- Table structure for six_article
-- ----------------------------
DROP TABLE IF EXISTS `six_article`;
CREATE TABLE `six_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '用户的id',
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '作者',
  `cid` int(10) UNSIGNED NOT NULL COMMENT '分类的id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章的标题',
  `summary` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章的摘要',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章的内容',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章缩略图',
  `comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览数',
  `likes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞数',
  `status` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '文章的状态 1发布 0禁用',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '文章的创建的时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '文章的删除的时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '文章的更新的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_article
-- ----------------------------
INSERT INTO `six_article` VALUES (1, 1, 'friends', 1, 'swoole(3)网络服务模型', '一:单进程阻塞', '<p><br></p><p>设计流程:</p><ol><li>创建一个socket,绑定端口bind,监听端口listen</li><li>进入while循环,阻塞在accept操作上,等待客户端连接进入,进入睡眠状态,直到有新的客户发起connet到服务器,accept函数返回客户端的socket</li><li>利用fread读取客户端socket当中的数据,收到数据后服务器程序进程处理,然后使用fwrite向客户端发送响应</li></ol><p><img src=\"https://img2020.cnblogs.com/i-beta/772876/202003/772876-20200303194319204-1897193505.png\" alt=\"\"></p>', '/storage/image/20200303\\c7c0a600608ef1e76716f569d5ed22f5.jpg', 0, 0, 0, 1, 1583240586, NULL, 1584694507);

-- ----------------------------
-- Table structure for six_category
-- ----------------------------
DROP TABLE IF EXISTS `six_category`;
CREATE TABLE `six_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文章分类名称',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '分类的创建的时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '分类的删除的时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '分类的更新的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '文章分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_category
-- ----------------------------
INSERT INTO `six_category` VALUES (1, '123123213', 1583236227, NULL, 1584694499);
INSERT INTO `six_category` VALUES (2, 'javasee', 1583236236, NULL, 1583405044);
INSERT INTO `six_category` VALUES (3, 'java', 1583405055, NULL, 1583405055);
INSERT INTO `six_category` VALUES (4, '图书', 1583405486, 1583405491, 1583405491);
INSERT INTO `six_category` VALUES (5, '33', 1584694463, NULL, 1584694463);

-- ----------------------------
-- Table structure for six_comment
-- ----------------------------
DROP TABLE IF EXISTS `six_comment`;
CREATE TABLE `six_comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tid` int(10) UNSIGNED NOT NULL COMMENT '评论的帖子',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '评论人的id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '评论的内容',
  `star` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论星级',
  `downs` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '附件下载次数',
  `recid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复的id',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '评论的的时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '评论删除的时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '评论修改的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评论表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for six_dashang
-- ----------------------------
DROP TABLE IF EXISTS `six_dashang`;
CREATE TABLE `six_dashang`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `account` int(10) UNSIGNED NOT NULL COMMENT '打赏的金额',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '打赏人',
  `aid` int(10) UNSIGNED NOT NULL COMMENT '打赏文章',
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '留言',
  `paystatus` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '支付状态 0 没有支付 1已支付',
  `create_time` int(10) UNSIGNED NOT NULL COMMENT '打赏时间',
  `orderNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '支付编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '打赏表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_dashang
-- ----------------------------
INSERT INTO `six_dashang` VALUES (1, 1, 1, 1, '很好', 0, 1587472390, '15874723901799');
INSERT INTO `six_dashang` VALUES (2, 1, 1, 1, '很好', 0, 1587472442, '15874724428544');
INSERT INTO `six_dashang` VALUES (3, 1, 1, 1, '很好', 0, 1587472641, '15874726419439');
INSERT INTO `six_dashang` VALUES (4, 1, 1, 1, '1111', 0, 1587472673, '15874726736501');
INSERT INTO `six_dashang` VALUES (5, 1, 1, 1, '1111', 0, 1587473077, '15874730772210');

-- ----------------------------
-- Table structure for six_module
-- ----------------------------
DROP TABLE IF EXISTS `six_module`;
CREATE TABLE `six_module`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '板块的名称',
  `introduce` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '板块的描述',
  `topics` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '主题数量',
  `click` int(10) UNSIGNED NULL DEFAULT 0 COMMENT '点击数量',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT '父级板块',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '板块创建的时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '板块删除的时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '板块更新的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '板块表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_module
-- ----------------------------
INSERT INTO `six_module` VALUES (1, 'javasee', '帅气', 0, 0, 0, 1583160827, NULL, 1583480601);
INSERT INTO `six_module` VALUES (2, 'php', '这里是phper开发者的天堂,更欢迎有兴趣的广大学者', 0, 0, 0, 1583161019, NULL, 1583161526);
INSERT INTO `six_module` VALUES (3, 'java', 'java开发者的天堂', 0, 0, 1, 1583161458, NULL, 1583161458);
INSERT INTO `six_module` VALUES (4, '文章', '文章展示', 0, 0, 0, 1586590307, NULL, 1586590307);

-- ----------------------------
-- Table structure for six_options
-- ----------------------------
DROP TABLE IF EXISTS `six_options`;
CREATE TABLE `six_options`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '选项名称',
  `value` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '选项值',
  `type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '选项的类型',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '网站的选项表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_options
-- ----------------------------
INSERT INTO `six_options` VALUES (1, 'siteTitle', '1111', 'base');
INSERT INTO `six_options` VALUES (2, 'siteIcp', '1111', 'base');
INSERT INTO `six_options` VALUES (3, 'siteKeywords', '1111', 'base');
INSERT INTO `six_options` VALUES (4, 'siteDes', '1111', 'base');
INSERT INTO `six_options` VALUES (5, 'notice', '1111', 'base');
INSERT INTO `six_options` VALUES (6, 'siteFooterJs', '1111', 'base');
INSERT INTO `six_options` VALUES (7, 'siteStatus', 'on', 'reg');
INSERT INTO `six_options` VALUES (8, 'closeContent', '马上要关闭网站了', 'reg');
INSERT INTO `six_options` VALUES (9, 'regStatus', 'on', 'reg');
INSERT INTO `six_options` VALUES (10, 'allowQQreg', 'on', 'reg');
INSERT INTO `six_options` VALUES (11, 'full', 'on', 'reg');
INSERT INTO `six_options` VALUES (12, 'regMail', 'on', 'reg');
INSERT INTO `six_options` VALUES (21, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (22, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (23, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (24, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (25, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (26, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (27, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (28, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (29, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (30, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (31, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (32, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (33, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (34, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (35, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (36, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (37, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (38, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (39, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (40, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (41, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (42, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (43, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (44, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (45, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (46, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (47, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (48, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (49, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (50, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (51, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (52, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (53, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (54, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (55, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (56, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (57, 'siteTitle', '1231231', 'base');
INSERT INTO `six_options` VALUES (58, 'siteIcp', '23123', 'base');
INSERT INTO `six_options` VALUES (59, 'siteKeywords', '312', 'base');
INSERT INTO `six_options` VALUES (60, 'siteDes', '3123', 'base');
INSERT INTO `six_options` VALUES (61, 'notice', '123213', 'base');
INSERT INTO `six_options` VALUES (62, 'siteFooterJs', '123123', 'base');
INSERT INTO `six_options` VALUES (63, 'closeContent', '123213', 'reg');
INSERT INTO `six_options` VALUES (64, 'regMail', 'on', 'reg');
INSERT INTO `six_options` VALUES (65, 'themeLayout', 'black', 'theme');
INSERT INTO `six_options` VALUES (66, 'themePrimary', 'cyan', 'theme');
INSERT INTO `six_options` VALUES (67, 'themeAccent', 'cyan', 'theme');
INSERT INTO `six_options` VALUES (68, 'discolour', 'true', 'theme');
INSERT INTO `six_options` VALUES (69, 'fromName', '刘波', 'mailBase');
INSERT INTO `six_options` VALUES (70, 'fromAdress', '1309507657@qq.com', 'mailBase');
INSERT INTO `six_options` VALUES (71, 'smtpHost', 'smtp.qq.com', 'mailBase');
INSERT INTO `six_options` VALUES (72, 'smtpPort', '465', 'mailBase');
INSERT INTO `six_options` VALUES (73, 'encriptionType', 'SSL', 'mailBase');
INSERT INTO `six_options` VALUES (74, 'smtpUser', '1309507657@qq.com', 'mailBase');
INSERT INTO `six_options` VALUES (75, 'smtpPass', 'xwxhnhxvchqmbadb', 'mailBase');
INSERT INTO `six_options` VALUES (76, 'replyTo', '1309507657@qq.com', 'mailBase');

-- ----------------------------
-- Table structure for six_privilege
-- ----------------------------
DROP TABLE IF EXISTS `six_privilege`;
CREATE TABLE `six_privilege`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pri_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `app_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用名称',
  `controller_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '控制器名称',
  `action_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '方法名称',
  `parent_id` int(10) UNSIGNED NOT NULL COMMENT '父级权限id',
  `delete_time` int(11) NULL DEFAULT NULL COMMENT '软删除字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_privilege
-- ----------------------------
INSERT INTO `six_privilege` VALUES (1, '管理员权限', 'empty', 'empty', 'empty', 0, NULL);
INSERT INTO `six_privilege` VALUES (2, '系统设置', 'empty', 'empty', 'empty', 0, NULL);
INSERT INTO `six_privilege` VALUES (3, '论坛管理', 'empty', 'empty', 'empty', 0, NULL);
INSERT INTO `six_privilege` VALUES (4, '角色管理', 'admin', 'role', 'index', 1, NULL);
INSERT INTO `six_privilege` VALUES (5, '上级权限', '上级权限', '上级权限', '上级权限', 3, 1582774262);

-- ----------------------------
-- Table structure for six_role
-- ----------------------------
DROP TABLE IF EXISTS `six_role`;
CREATE TABLE `six_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_role
-- ----------------------------
INSERT INTO `six_role` VALUES (24, '开发');
INSERT INTO `six_role` VALUES (25, '物流');

-- ----------------------------
-- Table structure for six_role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `six_role_privilege`;
CREATE TABLE `six_role_privilege`  (
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色id',
  `privilege_id` int(10) UNSIGNED NOT NULL COMMENT '权限id',
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `privilege_id`(`privilege_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_role_privilege
-- ----------------------------
INSERT INTO `six_role_privilege` VALUES (21, 1);
INSERT INTO `six_role_privilege` VALUES (21, 4);
INSERT INTO `six_role_privilege` VALUES (21, 2);
INSERT INTO `six_role_privilege` VALUES (21, 3);
INSERT INTO `six_role_privilege` VALUES (22, 1);
INSERT INTO `six_role_privilege` VALUES (22, 4);
INSERT INTO `six_role_privilege` VALUES (22, 2);
INSERT INTO `six_role_privilege` VALUES (22, 3);
INSERT INTO `six_role_privilege` VALUES (23, 1);
INSERT INTO `six_role_privilege` VALUES (23, 4);
INSERT INTO `six_role_privilege` VALUES (23, 2);
INSERT INTO `six_role_privilege` VALUES (23, 3);
INSERT INTO `six_role_privilege` VALUES (24, 1);
INSERT INTO `six_role_privilege` VALUES (24, 4);
INSERT INTO `six_role_privilege` VALUES (24, 2);
INSERT INTO `six_role_privilege` VALUES (24, 3);
INSERT INTO `six_role_privilege` VALUES (25, 1);
INSERT INTO `six_role_privilege` VALUES (25, 4);
INSERT INTO `six_role_privilege` VALUES (25, 2);
INSERT INTO `six_role_privilege` VALUES (25, 3);
INSERT INTO `six_role_privilege` VALUES (26, 1);
INSERT INTO `six_role_privilege` VALUES (26, 4);
INSERT INTO `six_role_privilege` VALUES (26, 2);
INSERT INTO `six_role_privilege` VALUES (26, 3);

-- ----------------------------
-- Table structure for six_topic
-- ----------------------------
DROP TABLE IF EXISTS `six_topic`;
CREATE TABLE `six_topic`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) UNSIGNED NOT NULL COMMENT '发帖人',
  `title` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '帖子标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '帖子的内容',
  `comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论的数量',
  `view` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览的数量',
  `likes` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞的数量',
  `close` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '帖子的状态 0 关闭 1 启用',
  `top` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '置顶 0 不置顶 1 置顶',
  `sign` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题附件标识',
  `mid` int(10) UNSIGNED NOT NULL COMMENT '模块的id',
  `create_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '帖子的创建的时间',
  `delete_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '帖子的删除的时间',
  `update_time` int(10) UNSIGNED NULL DEFAULT NULL COMMENT '帖子的更新的时间',
  `essence` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是精华 0 不是 1 是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '主题表(帖子)' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_topic
-- ----------------------------
INSERT INTO `six_topic` VALUES (2, 1, '三轮车列表', '<p>dasdqwvfsdfqwe</p>', 0, 0, 0, 1, 0, '', 1, 1585984050, NULL, 1585984050, 0);
INSERT INTO `six_topic` VALUES (3, 1, 'php', '<p>asdqwszdvgsddqaw</p>', 0, 0, 0, 1, 0, '', 2, 1585984543, NULL, 1585984543, 0);
INSERT INTO `six_topic` VALUES (4, 1, '三轮车列表', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078954, NULL, 1586078954, 0);
INSERT INTO `six_topic` VALUES (5, 1, '三轮车列表', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078955, NULL, 1586078955, 0);
INSERT INTO `six_topic` VALUES (6, 1, 'asdqweasdas', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078975, NULL, 1586078975, 0);
INSERT INTO `six_topic` VALUES (7, 1, 'asdqweasdas', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078982, NULL, 1586078982, 0);
INSERT INTO `six_topic` VALUES (8, 1, 'asdqweasdas', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078990, NULL, 1586078990, 0);
INSERT INTO `six_topic` VALUES (9, 1, 'asdqweasdas', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586078998, NULL, 1586078998, 0);
INSERT INTO `six_topic` VALUES (10, 1, 'asdqweasdas', '<p>sadwqe123dawqdaeqw123</p>', 0, 0, 0, 1, 0, 'UjLon0etvOglBW65rSI8abxhKuJqRF', 2, 1586079007, NULL, 1586079007, 0);
INSERT INTO `six_topic` VALUES (11, 1, 'php', '<p>asdqwedsdafqawe</p>', 0, 0, 0, 1, 0, 'C30ph95ER7AlXdj2JOxgqaGBTIV4fv', 1, 1586079032, NULL, 1586079032, 0);
INSERT INTO `six_topic` VALUES (12, 1, 'php', '<p>asdqwedsdafqawe</p>', 0, 0, 0, 1, 0, 'C30ph95ER7AlXdj2JOxgqaGBTIV4fv', 1, 1586079035, NULL, 1586079035, 0);
INSERT INTO `six_topic` VALUES (13, 1, 'php', '<p>asdqwedsdafqawe</p>', 0, 0, 0, 1, 0, 'C30ph95ER7AlXdj2JOxgqaGBTIV4fv', 1, 1586079037, NULL, 1586079037, 0);
INSERT INTO `six_topic` VALUES (14, 1, 'php', '<p>asdqwedsdafqawe</p>', 0, 0, 0, 1, 0, 'C30ph95ER7AlXdj2JOxgqaGBTIV4fv', 1, 1586079039, NULL, 1586079039, 0);
INSERT INTO `six_topic` VALUES (15, 1, 'qwedsadqewasd', '<p>asdqweasdqwe</p>', 0, 0, 0, 1, 1, 'ZIkUQhTbHzS01yfYvsgwqJW3ax2E9D', 2, 1586249584, NULL, 1586249584, 1);

-- ----------------------------
-- Table structure for six_user
-- ----------------------------
DROP TABLE IF EXISTS `six_user`;
CREATE TABLE `six_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '管理员名称',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户的头像',
  `last_ip` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '127.0.0.1' COMMENT '最后一次登录的IP',
  `last_time` int(10) UNSIGNED NOT NULL DEFAULT 1573567992 COMMENT '最后一次登录的时间',
  `add_time` int(10) UNSIGNED NOT NULL COMMENT '创建账户的时间',
  `topics` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '主题数',
  `comments` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '评论数',
  `essences` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '精华数',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of six_user
-- ----------------------------
INSERT INTO `six_user` VALUES (1, 'jake-ma', 'edb1cd41cbd3e8433c0acd0e4c1ea734', '1309507657@qq.com', '/storage/user/20200407\\fa46f7760186783e86f577ec643c839a.jpg', '127.0.0.1', 1587472657, 1585743878, 1, 0, 0);

SET FOREIGN_KEY_CHECKS = 1;
